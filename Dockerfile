FROM memcached:alpine

# Switch back to root user to upgrade packages.
USER root
# hadolint ignore=DL3017
RUN apk upgrade --no-cache

# End up the same user as the base image.
USER memcache
