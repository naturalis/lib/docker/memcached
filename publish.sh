#!/bin/sh
set -eu

image=$CI_REGISTRY_IMAGE:build_$CI_COMMIT_REF_SLUG

docker pull "$image"

docker tag "$image" "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID"

if [ "$CI_COMMIT_BRANCH" = "main" ]; then
  docker tag "$image" "$CI_REGISTRY_IMAGE:latest"
  docker push "$CI_REGISTRY_IMAGE:latest"
fi

docker push "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID"
